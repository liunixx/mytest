package khliu.tw.mytest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import khliu.tw.models.AppConfig;
import khliu.tw.util.NetRequest;
import okhttp3.Request;

public class MainActivity extends AppCompatActivity {

    private EditText et_account;
    private EditText et_passwd;
    private Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setViews();
    }

    private void initViews() {
         et_account=findViewById(R.id.et_account);
         et_passwd=findViewById(R.id.et_passwd);
         btn_login=findViewById(R.id.btn_login);

    }

    private void setViews() {
         btn_login.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 login();
             }
         });


    }

    public void login() {
            String sLoginid = et_account.getText().toString();
            String sPasswd = et_passwd.getText().toString();
            Map mpLogin = new HashMap();
            mpLogin.put("AccountID",sLoginid);
            mpLogin.put("AccountPWD",sPasswd);
            NetRequest.postFormRequest(this, AppConfig.loginURL, mpLogin, new NetRequest.DataCallBack() {
                @Override
                public void requestSuccess(String result) throws Exception {

                }

                @Override
                public void requestFailure(Request request, IOException e) {

                }
            });
    }

}
