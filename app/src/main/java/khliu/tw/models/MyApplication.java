package khliu.tw.models;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

// import cn.jpush.android.api.JPushInterface;

public class MyApplication extends Application {
    private static final String TAG = "MyApplication";
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        // JPushInterface.setDebugMode(true);
        // JPushInterface.init(this);

    }

    public static Context getContext() {
        return context;
    }

}