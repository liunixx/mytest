package khliu.tw.models;

public class AppConfig {

    // public static final String baseURL = "https://www.sharebonus.tw/";
    public static final String baseURL = "https://www.saja.com.tw/";
    

    public static final String prefixid="saja";
    //是否可点击标志
    public static boolean canClick = true;
    //登录
    public static final String loginURL = baseURL + "Get_login/login_check/";
    //充值确认
    public static final String rechargerUrl = baseURL + "get_deposit/pay_check/";
    //微信支付接口
    public static final String weiXinURL = baseURL + "get_deposit/wxpay_send/";
    //支付宝
    public static final String aliPayURL = baseURL + "get_deposit/alipay_send";
    //忘记密码
    public static final String getPW = baseURL + "Get_login/passwd_send/";
    //商家收款二维码
    public static final String storeReceiptCode = baseURL + "get_qrcode/gen_payto_user_qr/";
    //获取账户金额
    public static final String getCash = baseURL + "get_cash/index/";
    //提现
    public static final String drawCash = baseURL + "get_cash/cash_exchange/";
    //会员中心
    public static final String userCenter = baseURL + "get_member/member_center/";
    //头像存储位置，需要拼接
    public static final String userImageUrl = baseURL + "uploads/sp/";
    //上传头像
    public static final String upLoadImageUrl = baseURL + "ImgUp/upload_c2.php";
    //会员中心
    public static final String memberCenter = baseURL + "member_center/";
    //雪币总余额，及雪币每月统计清单（in与out）
    public static final String xueBiCount = baseURL + "get_member/bonus_List_all/";
    //雪币全查询
    public static final String xueBiAll = baseURL + "get_member/bonus_list_search_all";
    //雪币每月统计功能
    public static final String xueBiMonthBill = baseURL + "get_member/bonus_List_all/";
    //雪币记录清单（日期）
    public static final String xueBiRecordDate = baseURL + "get_member/bonus_list_search_by_day";
    //会员资料
    public static final String userData = baseURL + "get_user/user_data_app/";
    //修改基本资料
    public static final String notifyUserData = baseURL + "get_user/edit_user_data/";
    //修改门店公告
    public static final String notifyShopNotice = baseURL + "Get_user/edit_user_store_announcement/";
    //修改登录密码
    public static final String notifyLoginPW = baseURL + "get_user/upd_user_pwd/";
    //退出登录接口
    public static final String logoutURL = baseURL + "home/logout/";
    //发送验证码
    public static final String sendCheckCode = baseURL + "Get_login/checkno_send/";
    //忘记密码商家2.0版
    public static final String forgrtPW = baseURL + "Get_login/forget_pwd/";
    //支付信箱账号
    public static final String WIDseller_email = "alpay@sharebonus.com";
    //•	支付同步回傳功能 (微信,支付宝)
    public static final String passBackURL = baseURL + "get_deposit/pay_result/";
    //•	支付同步回傳功能 (微信,支付宝,异步通知接口)
    public static final String passBackURLAsync = baseURL + "get_deposit/pay_result_async/";
    //微信开放平台审核通过的应用APPID
    public static final String appid = "wxf87d7a03b4f40b32";
    //雪人家族统计接口
    public static final String xueRenFamilyCount = baseURL + "Get_family/family_count";
    //雪人家族资料接口
    public static final String xueRenFamilyDetail = baseURL + "Get_family/family_list/N/页数/";
    //订单全查询
    public static final String orderListSerchAll = baseURL + "get_order/order_list_search_all";
    //经营数据，今日实时数据
    public static final String operateData_todayURL = baseURL + "Get_analysis/index/";
    //经营数据，区间数据
    public static final String operateData_daysURL = baseURL + "Get_analysis/order_analysis/";
    //消息中心接口
    public static final String msgCenterURL = baseURL + "Get_message/message_center";
    //修改支付密码接口（就是修改兑换密码接口）
    public static final String modifyPayPW = baseURL + "get_user/upd_user_expwd/";
    //家族回馈接口（依商家统计）
    public static final String feedBackURL = baseURL + "get_member/FamilyBonusInSum";
    //台湾支付
    public static final String rechargeTWURL = baseURL + "get_deposit/creditcard_send/";
    //商家订单仪表盘数据接口
    public static final String busOrderDashboardURL = baseURL + "get_order/order_list_today/";
    //商家订单接口
    public static final String busOrderURL = baseURL + "get_order/order_list_today_scrollload/N/";
    //商家订单删除接口
    public static final String busOrderDeleteURL = baseURL + "TaiwanCash/pay_cancel/";
    //商家订单现金支付接口
    public static final String busOrderPayURL = baseURL + "TaiwanCash/pay_checkout/";
    //查询会员银行账号资料
    public static final String userBankInfoURL = baseURL + "Get_user/get_user_bank_info";
}