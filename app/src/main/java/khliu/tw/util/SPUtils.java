package khliu.tw.util;

import android.content.Context;
import android.content.SharedPreferences;

import khliu.tw.models.AppConfig;
import khliu.tw.models.MyApplication;

import com.google.gson.Gson;

public class SPUtils {
    private static final String TAG = "UserSPUtils";

    private static SharedPreferences userSP;

    //获取指定的sp
    private static SharedPreferences getUserSP() {
        userSP = new MyApplication().getContext().getSharedPreferences(AppConfig.prefixid, Context.MODE_PRIVATE);
        return userSP;
    }

    /* 向指定的user  sp中存储userbean对象

    public static void saveUserBean(UserBean bean) {
        getUserSP();
        //将bean转化为字符串存储
        Gson gson = new Gson();
        String mUserJson = gson.toJson(bean);
        SharedPreferences.Editor editor = userSP.edit();
        editor.putString("mUserJson", mUserJson);
        editor.commit();
    }
   */
    /**
     * 获取指定的sp中的指定user信息
     *
     * @return

    public static UserBean getUserbean() {
        getUserSP();
        String mUserJson = userSP.getString("mUserJson", "");
        //将字符串转为user对象
        Gson gson = new Gson();
        UserBean mUser = gson.fromJson(mUserJson, UserBean.class);
        return mUser;
    }
  */
    /**
     * 保存是否是第一次打开应用
     *
     * @param b 是否是第一次启动应用
     */
    public static void saveIsFirst(boolean b) {
        getUserSP();
        SharedPreferences.Editor editor = userSP.edit();
        editor.putBoolean("isFirst", b);
        editor.commit();
    }

    /**
     * 获取该应用是否是第一次启动
     *
     * @return true：表示是第一次，需要进入引导页；
     * false：表示不是第一次，不需要进入引导页
     */
    public static boolean getIsFirst() {
        getUserSP();
        boolean isFirst = userSP.getBoolean("isFirst", true);
        return isFirst;
    }

    //session请求操作
    public static void SaveSession(String sessionid) {
        getUserSP();
        SharedPreferences.Editor editor = userSP.edit();
        editor.putString("sessionid", sessionid);
        editor.commit();
    }

    public static String getSession() {
        getUserSP();
        String sessionid = userSP.getString("sessionid", "");
        return sessionid;
    }
}

