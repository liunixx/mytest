package khliu.tw.util;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.style.Circle;

import khliu.tw.models.AppConfig;
import khliu.tw.mytest.R;

/**
 * Created by Administrator on 2017/4/19 0019.
 */

public class ProgressBarUtil {

    private static FrameLayout rootContainer;
    private static ProgressBar spinKitView;

    public static ProgressBar createProgressBar(Activity activity) {
        // activity根部的ViewGroup，其实是一个FrameLayout

        rootContainer = (FrameLayout) activity.findViewById(android.R.id.content);
        // 给progressbar准备一个FrameLayout的LayoutParams
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        // 设置对其方式为：屏幕居中对其
        lp.gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
        /*
        创建要展示的View
         */
        spinKitView = new SpinKitView(activity);
        spinKitView.setLayoutParams(lp);
        Circle circle = new Circle();
        circle.setColor(R.color.colorAccent);
        spinKitView.setIndeterminateDrawable(circle);
        //设定背景形状与颜色
        spinKitView.setBackgroundResource(R.drawable.ic_launcher_background);
        /*
        将要展示的view挂到view树上
         */
        rootContainer.addView(spinKitView);
        return spinKitView;
    }

    public static void removeProgressBar() {
        if (spinKitView != null)
            rootContainer.removeView(spinKitView);
    }

    /**
     * 当前进度条所挂载的Activity中去掉进度条控件
     */
    // TODO: 2018/6/29 zzs
    public static void cancelProgressBar() {
        AppConfig.canClick = true;
        ProgressBarUtil.removeProgressBar();
    }

    /**
     * 去掉进度条效果
     */
    // TODO: 2018/6/29 zzs
    public static void startProgressBar(Activity mActivity) {
        //这是一个比较常见的bug，出现bug的原因是AlertDialog需要依赖context呈现到前台，
        // 但是依赖的activity已被销毁，即context不复存在了。
        if (!mActivity.isFinishing()) {
            ProgressBarUtil.createProgressBar(mActivity);
        }
    }
}